﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform playerTransform;
    public float ZPos;
    
    private void Update()
    {
        Vector3 playerPos = playerTransform.position;
        playerPos.z = ZPos;
        playerPos.y = 0;
        transform.position = playerPos;
    }
}
