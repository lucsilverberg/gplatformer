﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed = 5;
    public Rigidbody2D thisRigidbody;

    public float JumpForce;
    public SpriteRenderer thisSpriteRenderer;

    public Animator ThisAnimator;

    public Collider2D ThisCollider;
    public LayerMask GroundLayer;

    int TotalCherries = 0;

    public Text CherryAmountText;

    public float HurtForce;
    public enum EnumState
    {
        Idle = 0,
        Running = 1,
        Jumping = 2,
        Fall = 3,
        Hurt = 4,
        Win = 5,
        Lose = 6
    }

    public EnumState PlayerState = EnumState.Idle;

    public GameObject GameOverUI;
    public GameObject WinUI;

    private void Start()
    {
        TotalCherries = 0;
        UpdateTextJumlahCherry();
    }

    private void Update()
    {
        if (PlayerState != EnumState.Hurt && PlayerState != EnumState.Win && PlayerState != EnumState.Lose)
        {
            Move();

            //Flip sprite sesuai arah jika tombol A/D ditekan
            if (Input.GetAxisRaw("Horizontal") != 0)
                FlipSprite(Input.GetAxisRaw("Horizontal")<0);
        
            //Melompat jika karakter ada di tanah dan tombol "JUMP" ditekan
            if (Input.GetButtonDown("Jump") && ThisCollider.IsTouchingLayers(GroundLayer))
                Jump();
        }
        
        CheckVelocityState();
        SetIntAnimatorParameter("State", (int)PlayerState);
    }

    private void Jump()
    {
        thisRigidbody.velocity = new Vector2(thisRigidbody.velocity.x, JumpForce);
    }

    private void Move()
    {
        thisRigidbody.velocity = (new Vector2(Input.GetAxis("Horizontal") * MoveSpeed, thisRigidbody.velocity.y));
    }


    void FlipSprite(bool flag)
    {
        thisSpriteRenderer.flipX = flag;
    }

    void SetIntAnimatorParameter(string parameterID, int value)
    {
        ThisAnimator.SetInteger(parameterID, value);
    }

    void CheckVelocityState()
    {
        if(PlayerState == EnumState.Win || PlayerState == EnumState.Lose)
        {
            thisRigidbody.velocity = new Vector2(0, 0);
        }
        else if (PlayerState == EnumState.Hurt)
        {
            if (Mathf.Abs(thisRigidbody.velocity.x) < 0.1f)
            {
                PlayerState = EnumState.Idle;
            }
        }
        else if (thisRigidbody.velocity.y > 0.1f)
            PlayerState = EnumState.Jumping;
        else if(thisRigidbody.velocity.y <- 0.1f)
            PlayerState = EnumState.Fall;
        else if (Mathf.Abs(thisRigidbody.velocity.x) > 0.1f)
            PlayerState = EnumState.Running;
        else 
            PlayerState = EnumState.Idle;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Collectibles"))
        {
            Debug.Log("Asik dapet cherry");
            Destroy(other.gameObject);
            TotalCherries += 1;// x+=1 => x=x+1
            UpdateTextJumlahCherry();
        }
        else if(other.gameObject.CompareTag("Finish"))
        {
            Debug.Log("Finish");
            PlayerState = EnumState.Win;
            WinUI.SetActive(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (PlayerState == EnumState.Fall)
            {
                Jump();
                Destroy(other.gameObject);
            }
            else
            {
                PlayerState = EnumState.Hurt;
                if (other.gameObject.transform.position.x > this.gameObject.transform.position.x)
                {
                    //Enemy ada di sebelah kanan, hempaskan player ke kiri
                    thisRigidbody.velocity = (new Vector2(  -HurtForce, thisRigidbody.velocity.y));
                }
                else
                {
                    //Enemy ada di sebelah kiri, hempaskan player ke kanan
                    thisRigidbody.velocity = (new Vector2(  HurtForce, thisRigidbody.velocity.y));
                }
            }
        }else if(other.gameObject.CompareTag("Jurang"))
        {
            PlayerState = EnumState.Lose;
            GameOverUI.SetActive(true);
            Debug.Log("DED!!");
        }
    }

    void UpdateTextJumlahCherry()
    {
        CherryAmountText.text = TotalCherries.ToString();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
